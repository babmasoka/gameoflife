using GOL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GOLTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void InitializingBoard_WithZero_ShouldThrowInvalidOperation()
        {
            //Arrange
            var adapter = new BoardAdapter(0,0,9);
            //Assert
            var ex = Assert.ThrowsException<InvalidOperationException>(()=> adapter.InitializeBoard());
            //Assert
            Assert.AreEqual("Cannot initialize board with 0 rows or 0 columns", ex.Message);
        }

        [TestMethod]
        public void RunningGame_WithZero_NumberOfGenerations_ShouldThrowInvalidOperation()
        {
            //Arrange
            var adapter = new BoardAdapter(20, 20, 0);
            adapter.InitializeBoard();
            //Assert
            var ex = Assert.ThrowsException<InvalidOperationException>(() => adapter.RunGame());
            //Assert
            Assert.AreEqual("Cannot run game with 0 number of generations", ex.Message);
        }
    }
}
