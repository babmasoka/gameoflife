﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace GOL
{
    public class BoardAdapter
    {
        private readonly int BoardRows;

        private readonly int BoardColumns;

        private Status[,] Board;

        private readonly int NumberOfGenerations;
        public BoardAdapter(int rows, int columns, int numOfGenerations)
        {
            BoardRows = rows;
            BoardColumns = columns;
            NumberOfGenerations = numOfGenerations;
        }
        public void InitializeBoard()
        {
            if (BoardRows <= 0 || BoardColumns <= 0)
            {
                throw new InvalidOperationException("Cannot initialize board with 0 rows or 0 columns");
            }
            Board = new Status[BoardRows, BoardColumns];
            // randomly initialize states
            for (var row = 0; row < BoardRows; row++)
                for (var column = 0; column < BoardColumns; column++)
                {
                    Board[row, column] = (Status)new Random().Next(0, 2);
                }
        }

        public void RunGame()
        {
            Console.Clear();
            if (NumberOfGenerations <= 0)
            {
                throw new InvalidOperationException("Cannot run game with 0 number of generations");
            }
            // Displaying the board 
            for (int i = 0; i < NumberOfGenerations; i++)
            {
                PrintBoard(Board);
                Board = NextGeneration(Board);
            }
        }

        private  Status[,] NextGeneration(Status[,] currentGrid)
        {
            var nextGeneration = new Status[BoardRows, BoardColumns];

            // Loop through every cell 
            for (var row = 1; row < BoardRows - 1; row++)
                for (var column = 1; column < BoardColumns - 1; column++)
                {
                    // find your alive neighbors
                    var aliveNeighbors = 0;
                    for (var i = -1; i <= 1; i++)
                    {
                        for (var j = -1; j <= 1; j++)
                        {
                            aliveNeighbors += currentGrid[row + i, column + j] == Status.Alive ? 1 : 0;
                        }
                    }

                    var currentCell = currentGrid[row, column];

                    // The cuuent cell needs to be subtracted 
                    // from its neighbours as it was  
                    // counted before 
                    aliveNeighbors -= currentCell == Status.Alive ? 1 : 0;

                    // Implementing the Rules of Life 
                    // Cell is lonely and dies 
                    if (currentCell.HasFlag(Status.Alive) && aliveNeighbors < 2)
                    {
                        nextGeneration[row, column] = Status.Dead;
                    }
                    // Cell dies due to over population 
                    else if (currentCell.HasFlag(Status.Alive) && aliveNeighbors > 3)
                    {
                        nextGeneration[row, column] = Status.Dead;
                    }
                    // A new cell is born 
                    else if (currentCell.HasFlag(Status.Dead) && aliveNeighbors == 3)
                    {
                        nextGeneration[row, column] = Status.Alive;
                    }
                    // stays the same
                    else
                    {
                        nextGeneration[row, column] = currentCell;
                    }
                }
            return nextGeneration;
        }

        private void PrintBoard(Status[,] future, int timeout = 500)
        {
            var stringBuilder = new StringBuilder();
            for (var row = 0; row < BoardRows; row++)
            {
                for (var column = 0; column < BoardColumns; column++)
                {
                    var cell = future[row, column];
                    stringBuilder.Append(cell == Status.Alive ? "x" : "-");
                }
                stringBuilder.Append("\n");
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.CursorVisible = false;
            Console.SetCursorPosition(0, 0);
            Console.Write(stringBuilder.ToString());
            Thread.Sleep(timeout);
        }
    }
}
