﻿using System;

namespace GOL
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pleas Capture Board Size: (e.g. 15,20 .... represents 15 Rows and 20 Colmns");
            var boardSize = Console.ReadLine();
            Console.WriteLine("Please Capture Number of Generations");
            var numOfGenerations = Console.ReadLine();
            var boardConfig = boardSize.Split(',');
            var adapter = new BoardAdapter(int.Parse(boardConfig[0]), int.Parse(boardConfig[1]), int.Parse(numOfGenerations));

            adapter.InitializeBoard();

            adapter.RunGame();
        }
    }
}
